public abstract class Baking
{
    private double bakeTime;
    private int bakeTemp;
    private int timeCount = 1;
    private int tempCount = 1;

    public Bread(double bakeTime, int bakeTemp, int size)
    {
	this.bakeTime = bakeTime;
	this.bakeTemp = bakeTemp;
    }

    public double getBakeTime()
    {
	return bakeTime;
    }

    public int getBakeTemp()
    {
	return bakeTemp;
    }

    public void setBakeTime(double bakeTime)
    {
	this.bakeTime = bakeTime;
	timeCount++;
    }

    public void setBakeTemp(int bakeTemp)
    {
	this.bakeTemp = bakeTemp;
	tempCount++;
    }

    public static void bakeCheck()
    {
	if(timeCount == 1)
	{
	    double timeHolderOne = this.bakeTime;
	}
	else
	{
	    double timeHolderTwo = this.bakeTime;
	}

	if(tempCount == 1)
	{
	    int tempHolderOne = this.bakeTemp;
	}
	else
	{
	    int tempHolderTwo = this.bakeTemp;
	}

	if((timeHolderOne < timeHolderTwo) && (tempHolderOne < tempHolderTwo))
	{
	    System.out.println("Your baked good are very burnt");
	}
	else if((timeHolderOne > timeHolderTwo) && (tempHolderOne > tempHolderTwo ))
	{
	    System.out.println("Your baked good are very under baked");
	}
	else if(((timeHolderOne < timeHolderTwo) && (tempHolderOne > tempHolderTwo)) || ((timeHolderOne > timeHolderTwo) && (tempHolderOne < tempHolderTwo)))
	{
	    System.out.println("I guess that works out but I'm not happy");
	}
	else if((timeHolderOne > timeHolderTwo) || (tempHolderOne > tempHolderTwo))
	{
	    System.out.println("Your baked good are under baked");
	}
	else if((timeHolderOne < timeHoldertwo) || (tempHolderOne < tempHolderTwo))
	{
	    System.out.println("Your baked good are burnt");
	}
	else
	{
	    System.out.println("You followed the recipe, well done!");
	}
	
    }
}

public class Bread extends Bread
{
    private int size;

    public SubClass(double bakeTime, int bakeTemp)
    {
	super(bakeTime, bakeTemp);
	size = 1;
    }

    public int getSize()
    {
	return size;
    }

    public static void proofBread()
    {
	size *= 2;
    }
}

public class Biscuit extends Bread
{
    private int thickness;
    private int number;

    public Biscuit(double bakeTime, int bakeTemp, int thickness)
    {
	super(bakeTime, bakeTemp);
	this.thickness = thickness;
    }

    public int getThickness()
    {
	return thickness;
    }

    public int getNumber()
    {
	return number;
    }

    public void setAttribute(int thickness)
    {
	this.thickness = thickness;
    }

    public static void cutBiscuits()
    {
	if(thickness == 1)
	{
	    number = 5;
	}
	else if(thickness == .75)
	{
	    number = 7;
	}
	else if(thickness == .50)
	{
	    number = 9;
	}
	else if(thickness == .25)
	{
	    number = 12;
	}
	else if(thickness < .25)
	{
	    System.out.println("The dough is too thin, roll your dough again.");
	}
	else
	{
	    System.out.println("The dough is too thick, roll your dough again.");
	}
    }
}

public class DriverClass
{

    public static void main(String[] args)
    {

	Bread baguette = new Bread(.67, 450) //40 minutes over 60 minutes is .67 or 2/3 of an hour
	System.out.println(baguette.getSize());
	baguette.proofBread();
	System.out.println(baguette.getSize());
    baguette.bakeCheck();

	System.out.print();

	bread biscuit = new Biscuit(.20, 425) //12 minutes over 60 minutes is .20 or 1/5 of an hour
	System.out.println(biscuit.getNumber());
    biscuit.bakeCheck();

    }
}
