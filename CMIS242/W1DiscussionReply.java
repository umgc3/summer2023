public class Student {
    private String name; // to store name of the student
    private int age; // to store the student's age
    private String gender; // to store the student's gender
    private String major; // to store the student's major
    private double gpa; // to store the student's grade point average
    private String[] courses; // to store the names of the courses the student is enrolled in
    private int examScore1;
    private int examScore2;
    private int examScore3;

    //Changes by Parker Teasdale
    
    private int avgScore;

    //

    public Student(String name, int age, String gender, String major, double gpa, String[] courses, int examScore1, int examScore2, int examScore3) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.major = major;
        this.gpa = gpa;
        this.courses = courses;
        this.examScore1 = examScore1;
        this.examScore2 = examScore2;
        this.examScore3 = examScore3;

	//
	
	avgScore = 0;

	//
    }

    //

    public int calculateAverageScore()
    {
	avgScore = (examScore1 + examScore2 + examScore3)/3;

	return avgScore;
    }

    //

    public char calculateGrade() {
        int averageScore = (examScore1 + examScore2 + examScore3) / 3;
        if (averageScore >= 90) {
            return 'A';
        } else if (averageScore >= 80) {
            return 'B';
        } else if (averageScore >= 70) {
            return 'C';
        } else if (averageScore >= 60) {
            return 'D';
        } else {
            return 'F';
        }
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getMajor() {
        return major;
    }

    public double getGPA() {
        return gpa;
    }

    public String[] getCourses() {
        return courses;
    }

    public void setGPA(double gpa) {
        this.gpa = gpa;
    }
    public int getExamScore1() {
        return this.examScore1;
    }
    public int getExamScore2() {
        return this.examScore2;
    }
    public int getExamScore3() {
        return this.examScore3;
    }
}

public class MyDriver {
    public static void main(String[] args) {
        String[] courses = {"Calculus", "Physics", "Computer Science"};
        Student student1 = new Student("Alice", 20, "Female", "Computer Science", 3.5, courses, 80, 75, 90);

        String[] courses2 = {"History", "English", "Psychology"};
        Student student2 = new Student("Bob", 19, "Male", "Psychology", 3.2, courses2, 85, 90, 80);

        System.out.println("My student's name is " + student1.getName());
        System.out.println("My student's age is " + student1.getAge());
        System.out.println("My student's gender is " + student1.getGender());
        System.out.println("My student's major is " + student1.getMajor());
        System.out.println("My student's GPA is " + student1.getGPA());
        System.out.println("My student's courses are:");
        for (String course : student1.getCourses()) {
            System.out.println("- " + course);
        }

        System.out.println("My student's exam scores are: " + student1.getExamScore1() + ", " + student1.getExamScore2() + ", " + student1.getExamScore3());

	//
	System.out.println("My student's average exam score is: " + student1.calculateAverageScore());
	//

        System.out.println("My student's grade is " + student1.calculateGrade());

        // Update the student's GPA
        student1.setGPA(3.7);
        System.out.println("My student's updated GPA is " + student1.getGPA());

        System.out.println();
        System.out.println("Student 2's name is " + student2.getName());
        System.out.println("Student 2's age is " + student2.getAge());
        System.out.println("Student 2's gender is " + student2.getGender());
        System.out.println("Student 2's major is " + student2.getMajor());
        System.out.println("Student 2's GPA is " + student2.getGPA());
        System.out.println("Student 2's courses are:");
        for (String course : student2.getCourses()) {
            System.out.println("- " + course);
        }

        System.out.println("Student 2's exam scores are: " + student2.getExamScore1() + ", " + student2.getExamScore2() + ", " + student2.getExamScore3());

	//
	System.out.println("Student 2's average exam score is: " + student2.calculateAverageScore());
	//

        System.out.println("Student 2's grade is " + student2.calculateGrade());
    }
}
