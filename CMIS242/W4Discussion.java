package edu.umgc.cmis141.teasdale.testprogram;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class FileChooser
{
	
	//Choosing to have these as attributes for accesibility
    private JFrame frame;
    private JPanel panel;
    
    /**
     * Bundling the creation of the GUI into a method to call later
     */
    private void createAndShowGUI()
    {
    	//Creating a new frame and giving it a name to display
        frame = new JFrame("File Chooser Example");
        
        //Setting a default action when attempting to exit
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Creating a new panel to go into the frame
        panel = new JPanel();
        
        //Setting the layout
        panel.setLayout(new FlowLayout());

        //Creating a new button
        JButton openButton = new JButton("Open");
        
        //Adding the action listener to the button
        openButton.addActionListener(new OpenButtonListener());

        //Adding the button to the panel
        panel.add(openButton);
        
        //Adding the panel to the frame
        frame.add(panel);
        
        //Setting size
        frame.setSize(100, 100);
        
        //Setting GUI to be visible
        frame.setVisible(true);
    }
    
    /**
     * Action to be performed when the openFile button is selected
     */
    private void openFile()
    {
    	//Creating a filechooser object
        JFileChooser fileChooser = new JFileChooser();
        
        /*
         * Store user choice, showOpenDialog() returns an interger value to represent user choice
         * JFileChooser.APPROVE_OPTION is 0, user has selected a file and clicked the open button
         * JFileChooser.CANCEL_OPTION is 1, user has cancelled the dialog by using the cancel button or closing the dialog
         * JFileChooser.ERROR_OPTION is 2, an error has occured or the dialog was dismissed
         */
        int option = fileChooser.showOpenDialog(panel);
        
        /*
         * The user selects an option, if it is a directory then use recursion to be able to open another directory
         * If it is a file then call the method to display a method showing the file path
         */
        if (option == JFileChooser.APPROVE_OPTION)
        {
            File file = fileChooser.getSelectedFile();
            if (file.isDirectory())
            {
                openFile(); // Recursively open the selected directory
            }
            else 
            {
                showSelectedFile(file);
            }
        }
        //If no file selected then display the call the method to dispaly a message
        else
        {
            showNoFileSelected();
        }
    }
    
    //Method taking in a file object as a paramter, then printing out the file path
    private void showSelectedFile(File file)
    {
        JOptionPane.showMessageDialog(panel, "Selected file: " + file.getAbsolutePath());
    }

    //Method printing out that a file was not selected
    private void showNoFileSelected()
    {
        JOptionPane.showMessageDialog(panel, "No file selected");
    }

    /**
     * Main method
     * Create an isntance of the filechoose object
     * using the object call the createAndShowGUI method
     */
    public static void main(String[] args)
    {
    	FileChooser fileChooser = new FileChooser();
    	fileChooser.createAndShowGUI();
    }

    //Action listener that calls the method openFile()
    private class OpenButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            openFile();
        }
    }
}