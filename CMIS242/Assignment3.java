/**
 * 
 * @author Parker Teasdale
 * Date: 06/27/2023
 * Purpose:Creating a GUI with Distance and Temperature conversion button, as well as an exit button.
 */

/*
 * Public so child classes can inherit from it
 * Absttact so you cannot create objects off of it
 */
public abstract class Converter
{
	//private attribute for encapsulation
	private double input;

	/*
	 * Default constructor
	 * Assigns NaN to attribute
	 */
	public Converter()
	{
		input = Double.NaN;
	}

	/**
	 * Main constructor
	 * @param input
	 * Assings paramter to attribute
	 */
	Converter(double input)
	{
		this.input = input;
	}

	//Getter for accessibility
	public double getInput()
	{
		return input;
	}

	//Setter for accesibility
	public void setInput(double input)
	{
		this.input = input;	
	}

	//Method for returning the attribute
	public double convert()
	{
		return input;
	}
}

public class DistanceConverter extends Converter
{
	//Default constructor, inheriting from parent
	DistanceConverter()
	{
		super();
	}

	/**
	 * Main constructor, inheriting from parent
	 * @param input
	 */
	DistanceConverter(double input)
	{
		super(input);
	}

	/*
	 * Overiding the parent method
	 * Calls the parent method using super.method() and assigns the value to the input variable
	 * If a value was not provided, the default will assign the value NaN to the attribute
	 * In this case the method will return that value
	 * Other wise the method will pass the input into the formula and return the result
	 */
	@Override
	public double convert()
	{
		double input = super.convert();

		if(input == Double.NaN)
		{
			return input;
		}

		//Conversion formula: KM = M * 1.609

		return (input * 1.609);
	
	}
}

public class TemperatureConverter extends Converter
{
	//Default constructor, inheriting from parent
	TemperatureConverter()
	{
		super();
	}

	/**
	 * Main constructor, inheriting from parent
	 * @param input
	 */
	TemperatureConverter(double input)
	{
		super(input);
	}

	/*
	 * Overiding the parent method
	 * Calls the parent method using super.method() and assigns the value to the input variable
	 * If a value was not provided, the default will assign the value NaN to the attribute
	 * In this case the method will return that value
	 * Other wise the method will pass the input into the formula and return the result
	 */
	@Override
	public double convert()
	{
		double input = super.convert();

		if(input == Double.NaN)
		{
			return input;
		}

		//Conversion forumla: C = ((F-32)*5)/9

		return (((input-32)*5)/9);
		
	}
}

import java.awt.event.*;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;

public class GUIConverter extends JFrame implements ActionListener
{

	//setting these attributes for accessiblity in multiple methods
	private JFrame frame;
	private JPanel panel;
	private JPanel panelTwo;
    private JButton distanceButton;
    private JButton temperatureButton;
    private JButton exitButton;

    //I added the GUI creation into a constructor for cleanliness
	public GUIConverter()
	{

		//creating a new frame and providing text to display
		frame = new JFrame("Welcome to Converter");

		//setting default behavior when closing the frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//creating buttons and providing text to display
		distanceButton = new JButton("Distance Converter");
		temperatureButton = new JButton("Temperature Converter");
		exitButton = new JButton("Exit");

		//Associating and action listener with the buttons
		//Using the 'This' keyword to pass a reference of this class that is implementing action listener
		//The alternates would be to create an object of this class and pass that
		//Or create a new class and that hold the action listener method and reference it
		distanceButton.addActionListener(this);
		temperatureButton.addActionListener(this);
		exitButton.addActionListener(this);
		
		//Creating a new Jpanel
        panel = new JPanel();
        
        //Setting the panel layout scheme
        panel.setLayout(new BorderLayout());
        
        //Creating another panel
        panelTwo = new JPanel();
        
        //Setting the panel layout scheme
        panelTwo.setLayout(new GridLayout(1,2));
        
        /*
         * In order to acheieve a similar look to the provided example I needed to use multiple layour schemes
         * I started with a Border layout scheme and assigned the exit button to the south to fill the bottom of the frame
         * I then added a grid layout scheme and assigned the two conversion buttons to this to have them display side by side
         * To finish the look I assigned the buttons using the grid layout to occupy the center space of the border layout
         * Im sure there are endless combinations to acheive this, but this is the best way I could think of and find support of
         */
		panel.add(exitButton, BorderLayout.SOUTH);
		panelTwo.add(distanceButton);
		panelTwo.add(temperatureButton);
        panel.add(panelTwo, BorderLayout.CENTER);

		//As I have one panel added into another panel, I only need to add the higher level panel
		frame.add(panel);	
		
		//Setting size of the frame, this was more trial and error
		frame.setSize(500, 300);
		
		//This sets the frame to occupy the center of the screen
		frame.setLocationRelativeTo(null);

		//Setting the frame to be visilbe so it can be, uh, visible.
		frame.setVisible(true);
	}

	//This sections is for what happens when the distanceButton is selected
	public void distanceButton()
	{	
		//This displays an input dialog with the given text, the input is then assigned to the input variable
		String input = JOptionPane.showInputDialog(frame, "Input miles distance to convert");
		
		//Checking to see if the user selected the cancel or X button
		if(input != null)
		{
			//Checking to see if the user selected the OK button without entering a value
			if(input.equals(""))
			{
				//Making an object without paramter to call the default constructor
				DistanceConverter distanceButton = new DistanceConverter();
				//Showing the formatted result, using object.conver() to pass the input and have the NaN value returned
				JOptionPane.showMessageDialog(frame, input + " Miles equals " + String.format("%.4f", distanceButton.convert()) + " Kilometers");
			}
			else
			{
				/*
				*Parsing the string to a double and assigning the value to a variable
				*If it has made it to this point the it has been validated to be either a String
				*I cannot think of a way to validate a String 'number' vs. a String 'word'
				*The easiest way would be to use Try/Catch for the Exception in thread "AWT-EventQueue-0" error
				*/
				double validatedInput = Double.parseDouble(input);
				
				//Making an object with paramerts to call the main constructor
				DistanceConverter distanceButton = new DistanceConverter(validatedInput);
				//Making an object with paramerts to call the main constructor
				JOptionPane.showMessageDialog(frame, validatedInput + " Miles equals " + String.format("%.4f", distanceButton.convert()) + " Kilometers");
			}
		}
		else
		{
			//Do nothing
		}
			
	}

	//This sections is for what happens when the temperatureButton is selected
	public void temperatureButton()
	{
		//This displays an input dialog with the given text, the input is then assigned to the input variable
		String input = JOptionPane.showInputDialog(frame, "Input farenheit temperature to convert");
		
		//Checking to see if the user selected the cancel or X button
		if(input != null)
		{
			//Checking to see if the user selected the OK button without entering a value
			if(input.equals(""))
			{
				//Making an object without paramter to call the default constructor
				TemperatureConverter temperatureButton = new TemperatureConverter();
				//Showing the formatted result, using object.conver() to pass the input and have the NaN value returned
				JOptionPane.showMessageDialog(frame, input + " F equals " + String.format("%.4f", temperatureButton.convert()) + " C");
			}
			else
			{
				/*
				*Parsing the string to a double and assigning the value to a variable
				*If it has made it to this point the it has been validated to be either a String
				*I cannot think of a way to validate a String 'number' vs. a String 'word'
				*The easiest way would be to use Try/Catch for the Exception in thread "AWT-EventQueue-0" error
				*/
				double validatedInput = Double.parseDouble(input);
				//Making an object with paramerts to call the main constructor
				TemperatureConverter temperatureButton = new TemperatureConverter(validatedInput);
				//Making an object with paramerts to call the main constructor
				JOptionPane.showMessageDialog(frame, validatedInput + " F equals " + String.format("%.4f", temperatureButton.convert()) + " C");
			}
		}
		else
		{
			//Do nothing
		}
		
		
	}

	//This section is for what happens when the exitButton is selected
	public void exitButton()
	{
		//Close the program
		System.exit(0);
	}	

	//Overiding the parent class
    @Override
    /*
     * Action listener method, it is associated with the JButtons
     */
	public void actionPerformed(ActionEvent e)
	{
    	//Assigning the source of an event to an object for a cleaner look
        Object obj = e.getSource();

        //If the source is from the distanceButton, the distanceButton method is called
        //This pattern repeats for the other buttons
		if(obj == distanceButton)
		{
			distanceButton();
		}
		else if(obj == temperatureButton)
		{
			temperatureButton();
		}
		else
		{
			exitButton();
		}
	}
	
    //My method only includes one object creation which calls the constructor
	public static void main(String[] args)
	{
		GUIConverter app = new GUIConverter();
	}
}