/**
 * Author: Parker Teasdale
 * Date: 06/22/2023
 * Purpose: Snack ordering system
 */

package edu.umgc.cmis141.teasdale.testprogram;

public abstract class SnackClass
{
	
	//Parent class attributes
	private String id;
	private char size;
	private double price;

	//Overloading with a default constructor
	public SnackClass()
	{
		id = "";
		size = 0;
		price = 0;
	}

	//Main constructor
	public SnackClass(String id, char size)
	{
		this.id = id;
		this.size = size;
	}

	//Get methods for attributes
	public String getID()
	{
		return id;
	}

	public char getSize()
	{
		return size;
	}

	public double getPrice()
	{
		return price;
	}

	//Set methods for attributes
	public void setID(String id)
	{
		this.id = id;
	}

	public void setSize(char size)
	{
		this.size = size;
	}

	public void setPrice(double price)
	{
		this.price = price;
	}

	//Method to assign a value to price depending on the user size choice
	public void calculatePrice()
	{
		if(size == 'S')
		{
			price = 19.99;
		}
		else if (size == 'M')
		{
			price = 29.99;
		}
		else
		{
			price = 39.99;
		}
	}

	//Required toString method
	public String toString()
	{
		return "Your snack: type: " + size + ", id: " + id + " and price = " + String.format("%.2f",price);
	}
}

/**
 * Author: Parker Teasdale
 * Date: 06/22/2023
 * Purpose: Snack ordering system
 */

package edu.umgc.cmis141.teasdale.testprogram;

public class FruitSnack extends SnackClass
{
	//Child class attribute
	private boolean containsCitrus;

	//Using super() without paramters to call default parent constructor for default child constructor
	//Default constructor to demonstrate overloading constructors
	public FruitSnack()
	{
		super();
		this.containsCitrus = false;
	}
	
	//Main constructor
	public FruitSnack(String id, char size, boolean containsCitrus)
	{
		super(id, size);
		this.containsCitrus = containsCitrus;
		
		calculatePrice();
	}

	//Get method for child attribute
	public boolean getContainsCitrus()
	{
		return containsCitrus;
	}

	//Set method for child attribute
	public void setContainsCitrus(boolean containsCitrus)
	{
		this.containsCitrus = containsCitrus;
	}

	//uses super.metod() to call parent class method, then adds a set value to the current price
	//depending on if the boolean is true, if false then nothing happens
	@Override
	public void calculatePrice()
	{
		
		super.calculatePrice();
		
		if(containsCitrus)
		{
			double priceHolder = getPrice();
			
			 setPrice(priceHolder += 5.99);
		}
	}

	//Overriding parent toString method to add on the snack type 
	@Override
	public String toString()
	{
		return "You have chosen snack type = Fruit Snack, of type = \n" 
				+ getSize() + ", id = " + getID() + " and price = " + String.format("%.2f", getPrice());
	}
}

/**
 * Author: Parker Teasdale
 * Date: 06/22/2023
 * Purpose: Snack ordering system
 */

package edu.umgc.cmis141.teasdale.testprogram;

public class SaltySnack extends SnackClass
{
	//Child class attribute
	private boolean containsNuts;
	
	//Using super() without paramters to call default parent constructor for default child constructor
	//Default constructor to demonstrate overloading constructors
	public SaltySnack()
	{
		super();
		this.containsNuts = false;
	}

	//Main constructor
	public SaltySnack(String id, char size, boolean containsNuts)
	{
		super(id, size);
		this.containsNuts = containsNuts;
		
		calculatePrice();
	}

	//Get method for child attribute
	public boolean getContainsNuts()
	{
		return containsNuts;
	}

	//Set method for child attribute
	public void setContainsNuts(boolean containsNuts)
	{
		this.containsNuts = containsNuts;
	}

	//uses super.metod() to call parent class method, then adds a set value to the current price
	//depending on if the boolean is true, if false then nothing happens
	@Override
	public void calculatePrice()
	{
		super.calculatePrice();
		
		if(containsNuts)
		{
			double priceHolder = getPrice();
			
			setPrice(priceHolder += 4.50);
		}
	}

	//Overriding parent toString method to add on the snack type 
	@Override
	public String toString()
	{
		return "You have chosen snack type = Salty Snack, of type = \n" 
				+ getSize() + ", id = " + getID() + " and price = " + String.format("%.2f", getPrice());
	}
}

/**
 * Author: Parker Teasdale
 * Date: 06/22/2023
 * Purpose: Snack ordering system
 */

package edu.umgc.cmis141.teasdale.testprogram;

import java.util.Scanner;

public class OrderSystem
{
	//Separating all reusable print statements into 'menus', they are in sequential order
	
	//Main menu for initial choice of choosing a snack or leaving
	public static void mainMenu()
	{
		System.out.println("MENU");
		System.out.println("1: Order a Snack");
		System.out.println("2: Exit program");
		System.out.print("Enter your selection: ");
	}

	//Snack menu for choosing between a FruitSnack object or a SaltySnack oboject
	public static void snackMenu()
	{
		System.out.println("");
		System.out.print("Do you want Fruit Snack (1) or Salty Snack (2): ");
	}

	//Size meny to choose between one of three sizes, setting a price depending on the choice.
	public static void sizeMenu()
	{
		System.out.println("");
		System.out.println("What size do you want: S, M, L: ");
	}

	//Meny specially for FruitSnack objects, has you choose between the citrus add on or not
	public static void citrusMenu()
	{	
		System.out.println("");
		System.out.print("Do you want citrus fruit included? true/false: ");
	}

	//Meny specially for SaltySnack objects, has you choose between the nuts add on or not
	public static void nutsMenu()
	{
		System.out.println("");
		System.out.print("Do you want nuts included? true/false: ");
	}

	//Goodbye message, can been seen at the beginning with the exit choice, or at the end of chosoing a snack
	public static void exitMessage()
	{
		System.out.println("");
		System.out.println("Thank you for using the program. Goodbye!");
	}

	//Main method of the driver class
	public static void main(String[] args)
	{
		//Bunch of instance variables
		int menuChoice;
		char sizeChoice;
		boolean snackAddOnChoice;
		String id;

		//Creation of a new scanner object
		Scanner in = new Scanner(System.in);

		//Show main menu
		mainMenu();

		//Take user choice and store it in a variable
		menuChoice = in.nextInt();

		//Will continue further into more menus or display an exit menu depending on user choice stored in the variable
		if(menuChoice == 1)
		{
			//Show snack menu
			snackMenu();

			//Take user choice and store it in a variable, recycling the old variable to save memory
			menuChoice = in.nextInt();

			//Will begin the creation of a Fruit or Salty Snack object depending on user choice stored in the variable
			if(menuChoice == 1)
			{
				//Show size menu
				sizeMenu();	
				
				//Take user choice and store it in a variable
				sizeChoice = in.next().charAt(0);

				//Show citrus menu
				citrusMenu();
				
				//Take user choice and store it in a variable
				snackAddOnChoice = in.nextBoolean();
	
				//Determining if AddOns were added to the snack, this will change how the ID forms
				/*The ID is formed out of the following:
				 * FS = Fruit Snack - the snack type
				 * sizeChoice = how large of a snack it is (ex: S,M,L)
				 * WC/NC = With citrus or no citrus
				 * The string of numbers represents the date I added this
				 */
				if(snackAddOnChoice)
				{
					id = "FS" + sizeChoice + "WC06202023";
				}
				else
				{
					id = "FS" + sizeChoice + "NC06202023";
				}
				
				//Create a new snack object of the FruitSnack data type, the paramters come from the variables stored with user input
				FruitSnack snack = new FruitSnack(id, sizeChoice, snackAddOnChoice);

				//Using the toString method to display the snack attributes
				System.out.println("");
				System.out.println(snack.toString());

				//final goodbye
				exitMessage();
				
			}
			else
			{

				//Show size menu
				sizeMenu();
				
				//Take user choice and store it in a variable
				sizeChoice = in.next().charAt(0);

				//Show nuts menu
				nutsMenu();
				
				//Take user choice and store it in a variable
				snackAddOnChoice = in.nextBoolean();

				//Determining if AddOns were added to the snack, this will change how the ID forms
				/*The ID is formed out of the following:
				 * SS = Salty Snack - the snack type
				 * sizeChoice = how large of a snack it is (ex: S,M,L)
				 * WN/NN = With nuts or no nuts
				 * The string of numbers represents the date I added this
				 */
				if(snackAddOnChoice)
				{
					id = "SS" + sizeChoice + "WN06202023";
				}
				else
				{
					id = "SS" + sizeChoice + "NN06202023";
				}

				//Create a new snack object of the SaltySnack data type, the paramters come from the variables stored with user input
				SaltySnack snack = new SaltySnack(id, sizeChoice, snackAddOnChoice);

				//Using the toString method to display the snack attributes
				System.out.println("");
				System.out.println(snack.toString());

				//final goodbye
				exitMessage();
				
			}
		}
		else
		{
			//Goodbye message if the user does not wish to choose a snack
			exitMessage();
		}
		
		//closing the scanner object for memory
		in.close();
		
	}
}

