public class Tool
{
	public void use()
	{
		System.out.println("I can do anything!");
	}
}

public class Shovel extends Tool
{
	@Override
	public void use()
	{
		System.out.println("I can dig it");
	}
}

public class Axe extends Tool
{
	@Override
	public void use()
	{
		System.out.println("Hand axes, of stone, and used without handles (hafts) were the first axes.\n"
				+ "They had knapped (chipped) cutting edges of flint or other stone.\n"
				+ "Early examples of hand axes date back to 1.6 mya in the later Oldowan,[2] in Southern Ethiopia around 1.4 mya,[3] and in 1.2 mya deposits in Olduvai Gorge.\n"
				+ "[4] Stone axes made with ground cutting edges were first developed sometime in the late Pleistocene in Australia, where grind-edge axe fragments from sites\n"
				+ "in Arnhem Land date back at least 44,000 years;[5][6] grind-edge axes were later present in Japan some time around 38,000 BP, and are known from several Upper Palaeolithic sites\n"
				+ "on the islands of Honshu and Kyushu.[7] Hafted axes are first known from the Mesolithic period (c. 6000 BC). Few wooden hafts have been found from this period,\n"
				+ "but it seems that the axe was normally hafted by wedging. Birch-tar and rawhide lashings were used to fix the blade.\n"
				+ "\n"
				+ "The distribution of stone axes is an important indication of prehistoric trade.[8] Thin sectioning is used to determine the provenance of the stone blades.\n"
				+ "In Europe, Neolithic "axe factories", where thousands of ground stone axes were roughed out, are known from many places, such as:\n"
				+ "\n"
				+ "Great Langdale, England (tuff)\n"
				+ "Rathlin Island, Ireland (porcellanite)\n"
				+ "Krzemionki, Poland (flint)\n"
				+ "Neolithic flint mines of Spiennes, Belgium (flint)\n"
				+ "Plancher-les-Mines, France (pelite)\n"
				+ "Aosta Valley, Italy (omphacite).\n"
				+ "\n"
				+ "metal axes are still produced and in use today in parts of Papua, Indonesia. The Mount Hagen area of Papua New Guinea was an important production centre.\n"
				+ "\n"
				+ "From the late Neolithic/Chalcolithic onwards, axes were made of copper or copper mixed with arsenic.\n"
				+ "These axes were flat and hafted much like their stone predecessors. Axes continued to be made in this manner with the introduction of Bronze metallurgy.\n"
				+ "Eventually the hafting method changed and the flat axe developed into the "flanged axe", then palstaves, and later winged and socketed axes.");"
	}
}

public class Mousekatool
{
	public static void main(String[] args)
	{
		Shovel thingOne = new Shovel();
		Axe thingTwo = new Axe();

		thingOne.use(); //dynamic binding happens here
		thingTwo.use(); //dynamic binding happens here
	}
}