public class Candle
{

    //attributes
    private String color;
    private int height;
    private int diameter;
    private boolean isLit;

    //Constructor
    public Candle(String color, int height, int diameter)
    {
	if(height <= 0 || diameter <= 0)
	{
		System.out.println("Height and diamter must be greater than 0, no values have been set.");

		return; //this should be an exception
	}

	//set values
	this.color = color;
	this.height = height;
	this.diameter = diameter;

	//candle must be unlit to start
	this.isLit = false;

    }

    //method to light candle
    public void lightCandle()
    {
	if (isLit) //validaiton
	{
	    System.out.println("Candle is already lit");
	}
	else
	{
	    isLit = true;
	}
    }

    public void extinguishCandle()
    {
	if (!isLit) //validation
	{
	    System.out.println("Candle is already extinguished");
	}
	else
	{
	    isLit = false;
	}
    {

    public void setHeight(int height)
    {
	//validate
	if (height > this.height || height <= 0)
	{
	    System.out.println("New candle height must be less than current height but greater than -");
	}
	else
	{
	    this.height = height;
	}
    }

    public String toString()
    {
	return "Candle [color=" + color + ", height=" + height + ", diameter=" + diameter + ", isLit=" + isLit + "]";
    }
}

import java.util.ArrayList;
import java.util.Scanner;

public class HandleCandles
{

    ArrayList<Candle> list; //attribute to store a list of Candle objects

    //constructor
    public HandleCandles()
    {
	list = newArrayList<Candles>(); //creates new instance of an empty list
    }

    //method to display meny
    public static void candleMenu()
    {
	System.out.println("\n    Menu");
	System.out.println("1: Add candle");
	System.out.println("2: Remove candle");
	System.out.println("3: Light candle");
	System.out.println("4: Extinguish candle");
	System.out.println("5: Display Candles");
	System.out.println("9: Exit program");
    }

    //method to handle user's selection
    public void processChoice(int c)
    {
	switch (c)
	{
		case 1 :	addCandle();
				break;
		case 2 :	removeCandle();
				break;
		case 3 :	lightCandle(true);
				break;
		case 4 :	lightCandle(false);
				break;
		case 5 :	displayCandles()l
				break;
		case 9 :	System.out.printlnn("\nThank you for using the program, goodbye!");
				break;
		default:	System.out.println("Invalid choice");
	}
    }

    //method to display all candle ifnormation in the list
    private void displayCandles()
    {
	if (list.size() == 0)
	{
	    System.out.println("\nThere are no candles to display");
	}
	else
	{
	    System.out.println(); //empty line before candle data
	    for (int i = 0; i , list.size(); i++) //loop for all candles in the list
	    {
		Candle c = list.get(i); //get candle instance from the list for each x value
		
		System.out.println(c.toString()); //print candle data to console
	    }
	}
    }

    //when light = true, lit the candle and if light = false, extinguish the candle
    //TODO: could also first check if already lit or extinguished and give message
    private void lightCandle(boolean light)
    {
	Scanner in = new Scanner(System.in);

	//prompt user for data to find the candle to remove
	System.out.println("What is the height of the candle you want to light/extinguish");
	int height = in.nextInt();
	System.out.println("What is the color of the candle you want to light/extinguish");
	String color = in.next();

	//look for the candle that matches above
	int size = list.size(); //number of candles in the list
	System.out.println(); //empty line before candle data
	for (int i = 0; i< size; i++) //loop for all candles in the list
	{
	    Candle c = list.get(i); //get candle instance from the list for each index value
	

	    if(c.height == height && c.color.equalsIgnoreCase(color))
	    {
		if (light)
		{
		    c.isLit = true; //light candle
		}
		else
		{
		    c.isLit = false; //extinguish candle
		}
		
		list.set(i, c); //update in list
		System.out.println("\nLit/extinguished candle: " + c.toString()); //print candle data to console

		return; //done
	    }
	}

	//if here than did not find the canle
	System.out.println("\nThere is no candle with this height and color");
    }

    //method to remove the first candle that matches user's criteria
    private void removeCandle()
    {
	Scanner in = new Scanner(System.in);

	//prompt user for data to find the candle to remove
	System.out.println("What is the height of the candle you want to remove?");
	int height = in.nextInt();
	System.out.println("What is the color of the canlde you want to remove?");
	String color = in.next();

	//look for the candle that matches above
	int size = list.size(); //number of candles in the list
	System.out.println(); //empty line before candle data
	for (int i = 0; i < size; i++) //loop for all candles in the list
	{
	    Candle c = list.get(i); //get candle instance from the list for each index value

	    if (c.height == height && c.color.equalsIgnoreCase(color))
	    {
		list.remove(i); //remove the candle
		System.out.println("\nRemoved the following candle: " + c.toString()); //print candle data to console

		return; //done so can return from method
	    }
	}

	//if did not return from for=loop that means sit did not find the candle
	System.out.println("\nThere is no candle with this height and color");
    }


    private void addCandle()
    {
	Scanner in = new Scanner(System.in);

	//prompt user for all candle data to be set on creation
	System.out.println("What is the candle's height?");
	int height = in.nextInt();
	System.out.println("What is the candle's diamter?");
	double diameter = in.nextDouble();
	System.out.pritnln("What is the candle's color?");
	String color = in.next();

	//candle must start as unlit so no prompting

	//crate candle instance
	Candle c - new Candle(color, height, diameter);

	//tell user what was created
	System.out.println("\nThe following candle was created: " + c.toString());

	//add candle to instance list
	list.add(c);
    }

    public static void main(String[] args)
    {
	HandleCandles handler = new HandleCandles(); //new instance of driver class

	Scanner in = new Scanner(System.in);
	int selection = 0;

	do 
	{
	    handler.displayeMenu();

	    System.out.println("\nEnter your selection: ");
	    selection = in.nextInt();

	    handler.processChoice(selection);
	}
	while
	{
	    in.close();
	}
    }
}