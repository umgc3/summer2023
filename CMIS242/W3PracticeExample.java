/*
* abstract class to represent a generic media
*/
public abstract class Media
{
    //attributes
    private int id;
    private String title;
    private int yearPublished; //validate that 4 digits

    //constructor
    public Media(int id, String title, int yearPublished)
    {
	this.id = id;
	this.title = title;
	this.yearPublished = yearPublished;
    }

    //calculate rental fee, for generic media it is flat fee of $3.50
    public double rentalFee()
    {
	return 3.50;
    }

    //set methods
    public void setTitle(String title)
    {
	this.title = title;
    }

    public void setYearPublished(int yearPublished)
    {
	this.yearPublished = yearPublished;
    }

    //get methods
    public int getID()
    {
	return id;
    }

    public String getTitle()
    {
	return title;
    }

    public int getYearPublished()
    {
	return yearPublished;
    }
}

import java.util.Calendar;
public class EBook extends Media
{
    //local attributes
    private int numChapters;

    //constructor
    public EBook(int id, String title, int yearPublished, int numChapters)
    {
	super(id, title, yearPublished);
	this.numChapters = numChapters;
    }

    //set method
    public void setNumChapters(int numChapters)
    {
	this.numChapters = numChapters;
    }

    /get method
    public int getNumChapters()
    {
	return numChapters;
    }
	
    //override parent's
    @Override
    public double rentalFee()
    {
	double fee = numChapters * 0.10;
	int currentYear = Calendar.getInstance().get(Calendar.YEAR);

	if (this.getYear() == currentYear)
	{
	    fee += 1;
	}

	return fee;
    }

    @Override
    public String toString()
    {
	return "Ebook [id=" + getID() + ", title=" + getTitle()
		 + ", year=" + getYear() + ", chapters=" + numChapters + "]";
    }
}

import java.util.Calendar;
public class MusicCD extends Media
{
    //local attributes
    private int lengthMinutes;

    //constructor
    public MusicCD(int id, String title, int yearPublished, int lengthMinutes)
    {
	super(id, title, year);
	this.lengthMinutes = lengthMinutes;
    }

    //set method
    public void setLengthMinutes(int lengthMinutes)
    {
	this.lengthMinutes = lengthMinutes;
    }

    //get method
    public int getLengthMinutes()
    {
	return lengthMinutes;
    }

    //override parent's
    @Override
    public double rentalFee()
    {
	double fee = length * 0.02;
	int currentYear = Calendar.getInstance().get(Calendar.YEAR);

	if (this.getYear() == currentYear)
	{
	    fee += 1;
	}

	return fee;
    }

    @Override
    public String toString()
    {
	return "MusicCD [id=" + getID() + ", title-" + getTitle() + ", year="
			+ getYear() + ", length=" + lengthMinutes + "min]";
    }
}

public class MovieDVD extends Media
{
    //local attributes
    private double size;

    //constructor
    public MovieDVD(int id, String title, int yearPublished, double size)
    {
	super(id, title, year);
	this.size = size;
    }

    //set method
    public void setSize(double size)
    {
	this.size = size;
    }

    //get method
    public double getSize()
    {
	return size;
    }

    //inherits calculate rental fee method and no different calculation, no override

    @Override
    public String toString()
    {
	return "MovieDVD [ id=" + getID() + ", title=" + getTitle() + ", year="
			+ getYear() + ", size=" + size _ "MB]";
    }
}

public class RunRental
{
    public static void main(String[] args)
    {
	//create instances of the ebook and display
	EBook ebook = new EBook(123, "Forever Young", 2018, 20);
	System.out.print(ebook.toString());
	System.out.printf("    Rental fee=$%.2f\n", ebook.rentalFee());

	//create instances of the music cd and display
	MusicCD cd = new new MusicCD(124, "Beyond Today", 2020, 114);
	System.out.print(cd.toString());
	System.out.printf("    Rental fee=$%.2f\n", cd.rentalFee());

	//create instances of the movie dvd and display
	MovieDVD dvd = new MovieDVD(125, "After Tomorrow", 2020, 120);
	System.out.print(dvd.toString());
	System.out.printf("    New Rental fee=$%.2f\n", dvd.rentalFee());

	ebook.setNumChapters(25);
	System.out.print("\nChanging EBook chapters to 25: ");
	System.out.println(ebook.toString());
	System.out.printf(" New Rental fee=$%.2f\n", ebook.RentalFee());

	cd.setLength(120);
	System.out.print("\nChanging MusicCD length to 120: ");
	System.out.println(cd.toString());
	System.out.printf(" New Rental fee=$%.2f\n", cd.rentalFee());
    }
}
