public class CoveredInbox
{
    //attributes
    private boolean open;
    private int numLetters;

    //methods

    //check to see if inbox is open
    public boolean isOpen()
    {
	return open;
    }

    //get number of letters, but fails if inbox is closed
    public int getNumLetters()
    {
	if(!open)
	{
	    System.out.println("\nInbox is closed - cannot check number of letters");
	}

	return numLetters;
    }

    //method to open cover on the inbox
    public void openCover()
    {
	if(open)
	{
	    System.out.println("\nInbox is already opened");
	}
	else
	{
	    open = true;
	}
    }

    //method to close cover on the inbox
    public void closeCover()
    {
	if(!open)
	{
	    System.out.println("\nInbox is already closed");
	}
	else
	{
	    open = false;
	}
    }

    //method to add a letter to the inbox
    public void addLetter()
    {
	if(!open)
	{
	    System.out.println("\nCannot add letter - inbox is still closed");
	}
	else
	{
	    numLetters++;
	}
    }

    //method to remove letters from inbox
    public void removeLetters()
    {
	if(numLetters == 0)
	{
	    System.out.println("\nCannot remove letter - inbox is empty");
	}
	else if(!open)
	{
	    System.out.println("\nCannot remove letter - inbox is still closed");
	}
	else
	{
	    numLetters--;
	}
    }

    
}

import java.util.Scannerl

private class RunInbox
{
    private CoveredInbox inbox; //instance of the inbox

    //constructor
    public RunInbox()
    {
	inbox = new CoveredInbox();
    }

    //method to display the menu
    public void displayMenu()
    {
	System.out.println("\n    Menu");
	System.out.println("1: Open inbox ");
	System.out.println("2: Close inbox ");
	System.out.println("3: Add letter to inbox ");
	System.out.println("4: Remove letter from inbox");
	System.out.println("5: Check number of letter in inbox");
	System.out.println("9: Exit program");
    }

    //method to handle user's selection
    public void processChoice(int c)
    {
	switch (c)
	{
	    case 1 :	inbox.openCover();
			break;
	    case 2 :	inbox.closeCover();
			break;
	    case 3 :	inbox.addLetter();
			break;
	    case 4 :	inbox.removeLetter();
			break;
	    case 5 :	inbox.System.out.println("\nLetters in inbox = " + inbox.getNumLetters());
			break;
	    case 9 :	if(inbox.isOpen())
			{
			    System.out.println("\n You need to close the inbox before exiting");
			}
			else
			{
			    System.out.println("\nThank you for using the program, goodbye!");
			}
			break;
	    default:	System.out.println("Invalid chocie");		    
	}
    }

    public static void main(String[] args)
    {
	RunInbox run = new RunInbox(); //new instance

	Scanner in = new Scanner(System.in);
	int selectino = 0;

	do
	{
	    run.displayMenu();

	    System.out.println("\nEnter your selection: ");
	    selection = in.nextInt();

	    run.processChoice(selection);
	}
	while (selection != 9 || (selection == 9 && run.inbox.isOpen() == true))
	{
	    in.close();
	}
    }
}

