package thermo;

public class Thermostat {
	private double temp = 72;

	//Changes made by Parker Teasdale
	//New atribute for thermostat brand
	private String brand;
	
	//constructors
	//changing default constructor by assigning value to the new attribute when there are no paramters
	public Thermostat() {

	brand = "HoneyWell";

			}
	
	//Adding new atribute into the constructor's parameters
	public Thermostat(double temp, String brand) {
		this.temp = temp;
		this.brand = brand;
	}
	//setter for changing temp
	public void setTemp(double temp) {
		this.temp = temp;
	}
	
	//getter
	
	  public double getTemp(){
	  return temp;
	  }

	//getter for brand, no setter as I don't want brand to be changeable.
	public String getBrand()
	{
	    return brand;
	}
	 	
	//toString Override
	//Adding in new attribute to toString
	public String toString() {
		return "The current temperature is set to " + temp + " on your " + brand + " thermostat.";
	}
}

public class MyDriver {

	public static void main(String[] args) {
		//Filling the new paramter in
		Thermostat thermo = new Thermostat(68.2, Nest);
		
		System.out.println("The temperature is set to " + thermo.getTemp() + ".");
		thermo.setTemp(75.2);
		System.out.println("The temperature is set to " + thermo.getTemp() + ".");

		//Using get and toString methods to show new attribute
		System.out.println("Your have a " + thermo.getBrand() + "thermostat");
		System.out.println(thermo.toString());
		
	}

}
