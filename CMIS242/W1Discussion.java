public class Office
{
    public static void main(String[] args)
    {
	Monitor monitor = new Monitor();

	System.out.println(monitor.toString());

	monitor.powerOn();

	System.out.println();
	System.out.println(monitor.toString());
    }
}

public class Monitor
{
    private int size;
    private int pixelWidth;
    private int pixelHeight;
    private boolean power;

    public Monitor()
    {
	this.size = 27;
	this.pixelWidth = 1920;
	this.pixelHeight = 1080;
	this.power = false;
    }

    public Monitor(int size, int pixelWidth, int pixelHeight)
    {
	this.size = size;
	this.pixelWidth = pixelWidth;
	this.pixelHeight = pixelHeight;
	this.power = false;
    }

    public void powerOn()
    {
	if (power)
	{
	    System.out.println("The Monitor is already turned on");
	}
	else
	{
	    power = true;
	}
    }

    public void powerOff()
    {
	if(!power)
	{
	    System.out.println("The Monitor is already turned off");
	}
	else
	{
	    power = false;
	}
    }

    public String toString()
    {
    	return "Monitor:\nsize=" + size + "\nresolution=" + pixelWidth + "x" + pixelHeight + "\npower=" + power;
    }
}