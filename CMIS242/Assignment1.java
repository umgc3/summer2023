public class Weight
{
    private final double OUNCES_IN_A_POUND = 16.0;
    private int pounds;
    private double ounces;
    
    public Weight(int pounds, double ounces)
    {
    	this.pounds = pounds;
    	this.ounces = ounces;

    	normalize();	
    }
    
    private double toOunces()
    {
	double ounces = pounds * OUNCES_IN_A_POUND;

	double totalOunces = this.ounces + ounces;

	return totalOunces;
    }

    private void normalize()
    {
    	while(!(ounces < OUNCES_IN_A_POUND))
    	{
    		ounces -= OUNCES_IN_A_POUND;

    		pounds++;
    	}
	
    	System.out.println("Ounces is less than the amount of ounces in a pound");
    }

    public boolean lessThan(Weight weight)
    {
		boolean lessThan = toOunces() < weight.toOunces();
	return lessThan;
   	}

    public void addTo(Weight weight)
    {
	totalOunces = toOunces() + weight.toOunces();

	this.ounces = totalOunces;
	
	normalize();
    }

    public String toString()
    {
	return (pounds + " pounds " + String.format("and %.2f", ounces) + " ounces");
    }
}

public class Project
{
	
    private static Weight findMinimum(Weight weightOne, Weight weightTwo, Weight weightThree)
    {
    	Weight notFound = null;
    	
    	if(weightOne.lessThan(weightTwo) && weightOne.lessThan(weightThree))
    	{
    		return weightOne;
    	}
    	else if(weightTwo.lessThan(weightOne) && weightTwo.lessThan(weightThree))
    	{
    		return weightTwo;
    	}
    	else if(weightThree.lessThan(weightTwo) && weightThree.lessThan(weightOne))
    	{
    		return weightThree;
    	}
    	else
    	{
    		System.out.println("There are two weights that weigh the same");
    	}
    	
    	return notFound;
    }

    private static Weight findMaximum(Weight weightOne, Weight weightTwo, Weight weightThree)
    {
    	Weight notFound = null;
    	
    	if(!(weightOne.lessThan(weightTwo)) && !(weightOne.lessThan(weightThree)))
    	{
    		return weightOne;
    	}
    	else if(!(weightTwo.lessThan(weightOne)) && !(weightTwo.lessThan(weightThree)))
    	{
    		return weightTwo;
    	}
    	else if(!(weightThree.lessThan(weightTwo)) && !(weightThree.lessThan(weightOne)))
    	{
    		return weightThree;
    	}
    	else
    	{
    		System.out.println("There are two weights that weigh the same");
    	}
    	
    	return notFound;
    }

    
    private static Weight findAverage(Weight weightOne, Weight weightTwo, Weight weightThree)
    {
	double ounceAverage = (	weightOne.toOunces + weightTwo.toOunces + weightThree.toOunces)/3

	Weight weightAverage = new Weight(0, ounceAverage);

	weightAverage.normalize();	
	
	return weightAverage
    }

    public static void main(String[] args)
    {

	Weight weightOne = new Weight(11, 3);
	Weight weightTwo = new Weight(7, 20);
	Weight weightThree = new Weight(14, 6);
	
	System.out.println(findMinimum(weightOne, weightTwo, weightThree).toString());
	System.out.println(findMaximum(weightOne, weightTwo, weightThree).toString());
	System.out.println(findAverage(weightOne, weightTwo, weightThree).toString());

    }
}

