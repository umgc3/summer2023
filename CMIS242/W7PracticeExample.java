public class Pet
{
	private String name;
	private int age;

	pbulic Pet(String name, int age)
	{
		if (name == null || name.isBlank() || name.isEmpty())
		{
			throw new IllegalPetNameArgumentException(name);
		}

		if (age <= 0)
		{
			throw new IllegalPetAgeArgumentException(age);
		}

		this.name = name;
		this.age = age;
	}
}

public class IllegalPetNameArgumentException extends IllegalArgumentException
{
	private String msg;

	public IllegalPetNameArgumentException(String name)
	{
		if (name == null)
		{
			msg = "Pet name cannot be null"
		}
		else if (name.isBlank())
		{
			msg = "Pet name cannot have all blank values"
		}
		else if (name.isEmpty())
		{
			msg = "Pet name cannot be empty value"
		}
	}

	@Override
	public String toString()
	{
		return this.getClass().getSimpleName() + " : " + msg;
	}
}

public class IllegalPetAgeArgumentException extends IllegalArgumentException
{
	private int age

	public IllegalPetAgeArgumentException(int age)
	{
		super("Age must be greater than 0: age=" + age);
	}

	@Override
	public String toString()
	{
		return this.getClass().getSimpleName() + " : Age must be greater than 0: age=" + age;
	}
}

import java.util.Scanner;

public class CreatePet
{
	public static void main(String[] args)
	{
		Scanner scanstr = new Scanner(System.in);
		Scanner scanint = new Scanner(System.in);

		boolean invalidValue = true;
		String name = null;
		int age = 0;
		Pet pet = null;

		while(invalidValue)
		{
			System.out.print("\nEnter pet name: ");
			name = scanstr.nextLine();

			System.out.print("Enter pet age: ");
			age = scanint.nextInt();

			try
			{
				pet = new Pet(name, age);
				invalidValue = false;
			}
			catch (IllegalPetNameArgumentException e)
			{
				System.out.println("\nTry Again! Invalid pet name: " + e.getMessage());
			}
			catch (IllegalPetAgeArgumentException e2)
			{
				System.out.println("\nTry Again! Invalid pet age: " + e2.getMessage());
			}
		}

		System.out.println("\nSuccessfully create pet with name=" + pet.getName() + " age=" + pet.getAge());
	}
}